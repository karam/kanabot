set -e

l='app'

test ! -z "$BOT_TOKEN"

mv "$l/BOT.php" "$l/$BOT_TOKEN.php"
mv "$l/set_HOOK.php" "$l/set_$BOT_TOKEN.php"

vendor/bin/heroku-php-apache2 "$l"

