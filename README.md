# KanaBot

A simple Telegram bot to provide quizzes for the basic Japanese kana: both
hiragana and katakana.

Dedicated to Mina 🎁

### Sample instance

A sample instance is hosted for/at [@KanaQuizBot](https://t.me/KanaQuizBot),
the instance fully complies to this source tree.


## Deploying

Since the codebase is obviously *Heroku*-ready, I'll only provide instructions
for deploying on Heroku.  You have to modify the source code a little bit to be
able to use it outside the Heroku platform.

1. Register a new bot on Telegram and a new app on Heroku
1. Clone this repo and add your Heroku app's git remote
1. Set `BOT_TOKEN` config var to your Telegram bot token
1. Set `APP_NAME` config var to your Heroku app name
1. Push the code to Heroku

Don't forget to set the webhook by calling:
```sh
curl "https://$APP_NAME.herokuapp.com.com/set_$TOKEN.php"
```

Add the following commands to the bot:

```txt
start - (same as /help)
help - Send the help message
hiragana - Switch the quiz to Hiragana
katakana - Switch the quiz to Katakana
stop - Stops any ongoing quiz
```


## License

Licensed under the [MIT (Expat)](LICENSE) license.

> Copyright (C) 2021 Karam Assany (karam.assany@disroot.org)
