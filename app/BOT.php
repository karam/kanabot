<?php

/*
 * Copyright (C) 2021 Karam Assany <karam.assany@disroot.org>
 * Licensed under the MIT (Expat license.  See LICENSE
 */




/** Constants **/


$HELP_MESSAGE = "<b>Welcome to Kana Quiz Bot!</b>\n".
				"\n".
				"Send /hiragana to start a <i>hiragana</i> quiz\n".
				"Send /katakana to start a <i>katakana</i> quiz\n".
				"\n".
				'The quiz is unlimited and entirely random. No user-specfic'.
				" data are stored. The quiz will stop at inactivity (or when".
			   	" you send /stop), so you may have to start it again.\n".
				"\n".
				'Send /start or /help to get this message.';


$KANADICT = array(
		'hiragana' =>
			array(
				'あ' => 'a',
				'い' => 'i',
				'う' => 'u',
				'え' => 'e',
				'お' => 'o',
				'か' => 'ka',
				'き' => 'ki',
				'く' => 'ku',
				'け' => 'ke',
				'こ' => 'ko',
				'さ' => 'sa',
				'し' => 'shi',
				'す' => 'su',
				'せ' => 'se',
				'そ' => 'so',
				'た' => 'ta',
				'ち' => 'chi',
				'つ' => 'tsu',
				'て' => 'te',
				'と' => 'to',
				'な' => 'na',
				'に' => 'ni',
				'ぬ' => 'nu',
				'ね' => 'ne',
				'の' => 'no',
				'は' => 'ha',
				'ひ' => 'hi',
				'ふ' => 'fu',
				'へ' => 'he',
				'ほ' => 'ho',
				'ま' => 'ma',
				'み' => 'mi',
				'む' => 'mu',
				'め' => 'me',
				'も' => 'mo',
				'や' => 'ya',
				'ゆ' => 'yu',
				'よ' => 'yo',
				'ら' => 'ra',
				'り' => 'ri',
				'る' => 'ru',
				'れ' => 're',
				'ろ' => 'ro',
				'わ' => 'wa',
				'を' => 'o',
				'ん' => 'n'
			),
		'katakana' =>
			array(
				'ア' => 'a',
				'イ' => 'i',
				'ウ' => 'u',
				'エ' => 'e',
				'オ' => 'o',
				'カ' => 'ka',
				'キ' => 'ki',
				'ク' => 'ku',
				'ケ' => 'ke',
				'コ' => 'ko',
				'サ' => 'sa',
				'シ' => 'shi',
				'ス' => 'su',
				'セ' => 'se',
				'ソ' => 'so',
				'タ' => 'ta',
				'チ' => 'chi',
				'ツ' => 'tsu',
				'テ' => 'te',
				'ト' => 'to',
				'ナ' => 'na',
				'ニ' => 'ni',
				'ヌ' => 'nu',
				'ネ' => 'ne',
				'ノ' => 'no',
				'ハ' => 'ha',
				'ヒ' => 'hi',
				'フ' => 'fu',
				'ヘ' => 'he',
				'ホ' => 'ho',
				'マ' => 'ma',
				'ミ' => 'mi',
				'ム' => 'mu',
				'メ' => 'me',
				'モ' => 'mo',
				'ヤ' => 'ya',
				'ユ' => 'yu',
				'ヨ' => 'yo',
				'ラ' => 'ra',
				'リ' => 'ri',
				'ル' => 'ru',
				'レ' => 're',
				'ロ' => 'ro',
				'ワ' => 'wa',
				'ン' => 'n'

			)
	);



/** Functions **/


function api_call($method, $params) {

	global $token;

	$url = "https://api.telegram.org:443/bot$token/$method";

	$curl = curl_init($url);
	curl_setopt($curl, CURLOPT_HEADER, false);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_HTTPHEADER,
		array('Content-type: application/json'));
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($params));
	$response = curl_exec($curl);
	curl_close($curl);

	return json_decode($response, true);

};


function send_message($msg) {

	global $message;
	
	return api_call('sendMessage',
		array(
			'chat_id' => $message['chat']['id'],
			'text' => $msg,
			'disable_notification' => true,
			'parse_mode' => 'HTML'
		)
	);

};


function send_reply($msg) {

	global $message;
	
	api_call('sendMessage',
		array(
			'chat_id' => $message['chat']['id'],
			'text' => $msg,
			'disable_notification' => true,
			'parse_mode' => 'HTML',
			'reply_to_message_id' => $message['message_id'],
			'allow_sending_without_reply' => true
		)
	);

};


function quiz_next() {

	global $quiz_file;
	global $quiz_mode;
	global $KANADICT;

	$now = array_rand(
		$KANADICT[$quiz_mode]
	);

	send_message($now);

	file_put_contents($quiz_file,
		json_encode(
			array(
				'mode' => $quiz_mode,
				'current' => $now
			)
		)
	);

};


function start_quiz() {

	global $quiz_mode;

	send_message("You just started a <i>$quiz_mode</i> quiz.");

	quiz_next();

};



/** The main routine **/


// Get input and environment

$token = getenv('BOT_TOKEN');

$message = json_decode(file_get_contents('php://input'), true)['message'];


// Shortcuts

$text  = $message['text'];
$quiz_file = "quiz-{$message['chat']['id']}.json";


// Check on-going quiz existence

if (file_exists($quiz_file)) {
	$quiz_info = json_decode(file_get_contents($quiz_file), true);
	$quiz_mode = $quiz_info['mode'];
	$current = $quiz_info['current'];
}

else
	$quiz_mode = 'none';


// Check the messages' text

if ( ($text == '/start') or ($text == '/help') )
	send_message($HELP_MESSAGE);

else if ($text == '/stop')
{
	if ($quiz_mode == 'none')
		send_reply('<i>There is no ongoing quiz to stop.</i> See /help');
	else
	{
		unlink($quiz_file);
		send_reply('Stopped.');
	}
}

else if ($text == '/hiragana')
{
	$quiz_mode = 'hiragana';
	start_quiz();
}

else if ($text == '/katakana' )
{
	$quiz_mode = 'katakana';
	start_quiz();
}

else if ( $quiz_mode == 'none' )
	send_reply(
		'<i>Please make sure to start a quiz first.</i>'
		.' See /help');

else {

	$correct = $KANADICT[$quiz_mode][$current];

	if ( strtolower($text) == $correct )
		send_reply('<b>Correct!</b>');
	else
		send_reply("<b>Wrong!</b> It is \"$correct\"");

	quiz_next();

};
